import { atom } from 'recoil';

export const nodeTypeState = atom({
	key: 'nodeTypeState', // unique ID (with respect to other atoms/selectors)
	default: 'circle', // default value (aka initial value)
  });

export const layoutTypeState = atom({
    key: 'layoutTypeState', // unique ID (with respect to other atoms/selectors)
    default: 'pack', // default value (aka initial value)
});

export const nodeWidthState = atom({
  key: 'nodeWidthState', // unique ID (with respect to other atoms/selectors)
  default: 10, // default value (aka initial value)
});

export const nodeHeightState = atom({
  key: 'nodeHeightState', // node height
  default: 30, // default value (aka initial value)
});

export const nodeRatioState = atom({
  key: 'nodeRatioState', // unique ID (with respect to other atoms/selectors)
  default: 1, // default value (aka initial value)
});

export const nodePaddingState = atom({
  key: 'nodePaddingState', // unique ID (with respect to other atoms/selectors)
  default: 20, // default value (aka initial value)
});

export const heightState = atom({
  key: 'heightState', // unique ID (with respect to other atoms/selectors)
  default: 500, // default value (aka initial value)
});

// Enriched Nodes/Links Prior To Filtering
export const enrichedGraphState = atom({
  key: 'enricheGraphState', // unique ID (with respect to other atoms/selectors)
  default: null, // default value (aka initial value)
});

//Nodes which selected which may be a subset of visible nodes
export const contextGraphState = atom({
  key: 'contextGraphState', // unique ID (with respect to other atoms/selectors)
  default: null, // default value (aka initial value)
});

export const focusState = atom({
  key: 'focusState', // unique ID (with respect to other atoms/selectors)
  default: 0, // default value (aka initial value)
});


export const nodeFontState = atom({
  key: 'nodeFontState', // unique ID (with respect to other atoms/selectors)
  default: {size: 0.75}, // default value (aka initial value)
});

export const contextLevelState = atom({
  key: 'contextLevelState', // unique ID (with respect to other atoms/selectors)
  default: 1, // default value (aka initial value)
});


export const nodeFilterState = atom({
	key: 'nodeFilterState', // unique ID (with respect to other atoms/selectors)
	default: { }
  });

  export const linkFilterState = atom({
    key: 'linkFilterState', // unique ID (with respect to other atoms/selectors)
    default: { }
    });

  export const selectedNodeState = atom({
    key: 'selectedNodeState', // unique ID (with respect to other atoms/selectors)
    default: null, // default value (aka initial value)
  });

export const toggleState = atom({
  key: 'toggleState', // unique ID (with respect to other atoms/selectors)
  default: {
    toggleDetails: true,
    toggleGant: true,
    toggleAnimation: true
  }, // default value (aka initial value)
});

export const zoomState = atom({
  key: 'zoomState', // unique ID (with respect to other atoms/selectors)
  default: false, // default value (aka initial value)
});

export const theGraphState = atom({
  key: 'theGraphState', // unique ID (with respect to other atoms/selectors)
  default: null, // default value (aka initial value)
});

export const datasetsState = atom({
  key: 'datasetsState', // unique ID (with respect to other atoms/selectors)
  default: [], // default value (aka initial value)
});

export const legendState = {
  nodeTypes: []
};


export const showNodesState = atom({
  key: 'showNodesState', // unique ID (with respect to other atoms/selectors)
  default: true, // default value (aka initial value)
});

export const showLinksState = atom({
  key: 'showLinksState', // unique ID (with respect to other atoms/selectors)
  default: true, // default value (aka initial value)
});

export const labelState = atom({
  key: 'labelState', // unique ID (with respect to other atoms/selectors)
  default: false, // default value (aka initial value)
});
