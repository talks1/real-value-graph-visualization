import React, {useEffect,useState,useRef} from 'react';
import useResizeObserver from '../util/useResizeObserver';
import {extent,zoom,zoomIdentity,zoomTransform,range,select,selectAll,event,sum,min,max,scaleTime,scaleLinear,axisBottom,axisLeft,scaleOrdinal,schemeCategory10,scaleSequential,interpolateCool,transition} from 'd3';
import {useRecoilState,useRecoilValue,useSetRecoilState} from 'recoil';
import {enrichedGraphState,showNodesState, showLinksState,visibleNodesState,zoomState,reloadState,contextGraphState,selectedNodeState,nodeFilterState,nodeTypeState,layoutTypeState,heightState,widthState,nodeFontState,nodeWidthState,nodeHeightState,nodeRatioState,focusState,labelState,contextLevelState,nodePaddingState} from '../state/state';
import {deriveLevel, deriveLineage, surfaceLinks} from 'real-value-graph';

import {layout} from 'real-value-graph-layout';

let selectedNode = null;

let duration = 50;
let maxOffset = 40;
let percentageOffset = 0;

let animationEnabled=true;
export const useAnimation = ()=>{	
	return  [()=>animationEnabled,(b)=>{
		animationEnabled = b;
	}];
};

function updateDash() {
	selectAll('.g-arrow').style('stroke-dashoffset', percentageOffset * maxOffset);
	percentageOffset = percentageOffset <= 0 ? 1 : percentageOffset - 0.01;
	let configuredDuration = animationEnabled ? duration : 1000;	
	setTimeout(updateDash,configuredDuration);
}
//updateDash()


let contextNodes = [];
export const setContextNodes =(stack)=>contextNodes = stack;


let currentVisibleNodes = [];
export const SankeyFlowComponent =  ({graph, initialize=false, level=1,level1Type='Program',clazz='class1',width=1400,height=300,layoutType='circular-sankey',padding=10,filter=null}) => {

	const wrapperRef = useRef();
	const dimensions = useResizeObserver(wrapperRef);
	const svgRef = useRef();

	const [nodeFilter,setNodeFilter] = useRecoilState(nodeFilterState);
	const [nodeType,setNodeType] = useRecoilState(nodeTypeState);

	const nodeFont = useRecoilValue(nodeFontState);
	const [nodeWidth,setNodeWidth] = useRecoilState(nodeWidthState);
	const [nodeHeight,setNodeHeight] = useRecoilState(nodeHeightState);
	const [nodeRatio,setNodeRatio] = useRecoilState(nodeRatioState);	
	const [nodePadding,setNodePadding] = useRecoilState(nodePaddingState);
	const [contextLevel,setContextLevel] = useState(parseInt(level));
	const [focus,setFocus] = useRecoilState(focusState);

	
	const setSelectedNode = useSetRecoilState(selectedNodeState);	
	const setContextGraph = useSetRecoilState(contextGraphState);
	
	const setEnrichedGraph = useSetRecoilState(enrichedGraphState);

	const [zoomToFit,setZoomToFit] = useRecoilState(zoomState);
	const [visibleNodes,setVisibleNodes] = useState([]);

	const [showNodes,setShowNodes] = useRecoilState(showNodesState);	
	const [showLinks,setShowLinnks] = useRecoilState(showLinksState);
	const [showLabels,setShowLabels] = useRecoilState(labelState);
	
	const LABEL_OPACITY_SELECTED = 1;
	const LABEL_OPACITY_UNSELECTED = 0.4;
	const NODE_OPACITY_SELECTED = 1;
	const NODE_OPACITY_UNSELECTED = 0.4;
	const LINK_OPACITY_HIGHLIGHTED = 0.7;
	const LINK_OPACITY_SELECTED = 0.3;
	const LINK_OPACITY_UNSELECTED = 0.1;
	const MOTION_OPACITY_SELECTED = 0.7;
	const MOTION_OPACITY_UNSELECTED = 0.2;

	const cGraph = {
		nodes: graph ? graph.nodes.map(n=>Object.assign({},n)) : [],
		links: graph ? graph.links.map(l=>Object.assign({},l)) : []
	}; //copy graph

	console.log(cGraph);

	
	useEffect(()=>{
		
		const svg = select(`#${clazz}`);

		if (dimensions == null) return;

		svg.attr('width', `${dimensions.width > width ? dimensions.width: width}px`).attr('height', `${height}px`);

		let backgroundRect = svg.selectAll('.zoom-background');
		const zm = zoom().scaleExtent([0.1, 10]).on('zoom', zoomed);
		svg.call(zm);

		function zoomed() {
			backgroundRect.transition().duration(1000).attr('transform', event.transform);
		}
		function navigate(d) {
			if(currentVisibleNodes.indexOf(d.id)>=0){
				drillUp(d);
			}else{
				drillThrough(d);
			}
		}
		
		function drillThrough(d){
			//contextNodes.push(d)
			//setContextLevel(d.level+1)
			let vns = [...currentVisibleNodes];
			if(vns.indexOf(d.id)<0)
				vns.push(d.id);
			setVisibleNodes(vns);
		}
		function drillUp(d){
			// contextNodes.pop()
			// setContextLevel(d.level)
			let vns = [...currentVisibleNodes];
			let i = vns.indexOf(d.id);
			if(i>=0)
				vns.splice(i,1);
			setVisibleNodes(vns);
		}
		
		function selectNode(n){
			if (event.ctrlKey) {
				navigate(n);
			}else {
				if(selectedNode && n.id===selectedNode.id){
					selectedNode = null;
					hideNodeDetails(n);
				}else {
					svg.selectAll('rect').attr('fill', 'white');
					let idSel = `#${n.id}`;
					let nodeRect = svg.selectAll(idSel);
					nodeRect.attr('fill','gray');
					selectedNode = n;
					showNodeDetails(n);
				}
				setSelectedNode(selectedNode);
			}
		}

		function showNodeDetails (d) {
			let hNodes = highlightNodes(d);
			let node = backgroundRect.selectAll(`.${clazz}-node`);
			node.selectAll('rect').transition().duration(10).style('opacity', d=>hNodes[d.name] ? NODE_OPACITY_SELECTED : NODE_OPACITY_UNSELECTED);
			node.selectAll('text').transition().duration(10).style('opacity', d=>hNodes[d.name]? LABEL_OPACITY_SELECTED: LABEL_OPACITY_UNSELECTED);
			backgroundRect.selectAll(`.${clazz}-link`).transition().duration(10).style('opacity', l=>l.source.name == d.name || l.target.name == d.name ? LINK_OPACITY_HIGHLIGHTED : LINK_OPACITY_UNSELECTED);
			//backgroundRect.selectAll('.g-arrow').transition().duration(10).style('opacity', l=>l.source.name == d.name || l.target.name == d.name ? MOTION_OPACITY_SELECTED : MOTION_OPACITY_UNSELECTED)
		}
		function hideNodeDetails(d) {		
			backgroundRect.selectAll('rect').transition().duration(10).style('opacity', NODE_OPACITY_SELECTED);
			backgroundRect.selectAll(`.${clazz}-link`).transition().duration(10).style('opacity', LINK_OPACITY_SELECTED);
			backgroundRect.selectAll('text').transition().duration(10).style('opacity', LABEL_OPACITY_SELECTED);
			//backgroundRect.selectAll('.g-arrow').transition().duration(10).style('opacity',MOTION_OPACITY_SELECTED)
		}

		function highlightNodes(node) {
			let result = {};
			result[node.name]=1;
			node.sourceLinks.forEach(l=>{
				if(l.source.name == node.name) {
					result[l.target.name]=1;
				}
			});
			node.targetLinks.forEach(l=>{
				if(l.target.name == node.name) {
					result[l.source.name]=1;
				}
			});
			return result;
		}

		const colorScale = scaleOrdinal(schemeCategory10);

		function renderGraph(){

			currentVisibleNodes = visibleNodes;	

			if(initialize)
				backgroundRect.selectAll('*').remove();

			let cNodeMap = cGraph.nodes.reduce((h,n)=>{h[n.id]=true;return h;},{});
			
			//Enriched nodes with additional information to support filtering and layout
			let enrichedGraph = {nodes: cGraph.nodes, links: cGraph.links};
			enrichedGraph = deriveLevel(enrichedGraph);
			enrichedGraph = deriveLineage(enrichedGraph);
			enrichedGraph = surfaceLinks(enrichedGraph);

			setEnrichedGraph(enrichedGraph);
			//console.log(enrichedGraph.links)

			//Filtering of enriched nodes to visible nodes
			let vNodes = enrichedGraph.nodes
				.filter(n=>showNodes);
				//.filter(n=>filter || !cNodeMap[n.parentId] || visibleNodes.indexOf(n.parentId)>=0)
			
			Object.keys(nodeFilter).forEach(filter=>{				
				const filterFunction = nodeFilter[filter];				
				vNodes = vNodes.filter(filterFunction);
			});

			let vLinks = enrichedGraph.links
				.filter(l=>showLinks)
				.filter(l=>vNodes.find(n=>n.id===l.target) && vNodes.find(n=>n.id===l.source));
			
				
			setContextGraph({
				nodes: vNodes,
				links: vLinks
			});

			let layoutGraph = {
				nodes: vNodes,
				links: vLinks,
			};

			let viewGraph = layout({
				dimensions: [width,height],
				graph: layoutGraph,
				layoutType,
				nodeWidth,
				nodeHeight,
				nodeRatio, 
				nodePadding,
				padding
			});

			if(filter){
				layoutGraph = {
					nodes: viewGraph.nodes.filter(filter),
					links: vLinks, 
				};

				viewGraph = layout({
					dimensions: [width,height],
					graph: layoutGraph,
					layoutType,
					nodeWidth,
					nodeHeight,
					nodeRatio, 
					nodePadding,
					padding
				});
			}

		

			let vNodesMap = viewGraph.nodes.reduce((h,n)=>{h[n.id]=n;return h;},{});

			let minX = min(viewGraph.nodes,n=>n.x0);
			let maxX = max(viewGraph.nodes,n=>n.x1);
			let minY = min(viewGraph.nodes,n=>n.y0);
			let maxY = max(viewGraph.nodes,n=>n.y1);

			let cNodes = layoutGraph.nodes.reduce((h,n)=>{
				if(contextNodes.length===0 ? true : n.parentId===(contextNodes[contextNodes.length-1].id)){
					h[n.id]=n;
				}
				return h;
			},{});
				
			let cLinks = layoutGraph.links.reduce((h,l)=>{
					if(cNodes[l.target] && cNodes[l.source])
						h[l.key]=1;
					return h;
			},{});

			

			let cMinX = min(Object.values(cNodes),n=>n.x0);
			let cMaxX = max(Object.values(cNodes),n=>n.x1);
			let cMinY = min(Object.values(cNodes),n=>n.y0);
			let cMaxY = max(Object.values(cNodes),n=>n.y1);
		
			var scaleY =  dimensions.height/(maxY-minY);
			var scaleX =  dimensions.width/(maxX-minX);
			var scale = scaleX < scaleY ? scaleX : scaleY;
			
			if(zoomToFit){
				backgroundRect.transition().duration(3000).attr('transform', `translate(10 30) scale(${scale})`);
				setZoomToFit(false);
			}
		
			

			const costMin = min(viewGraph.nodes, d=>d.cost );
			const costMax = max(viewGraph.nodes, d=>d.cost );
			const costRange = costMax-costMin;
			//const costScale = d => parseInt(d.cost===undefined ? 1 : nodeHeight/2+(d.y1-d.y0)*d.cost/costRange)
			const costScale = d=>1;
									
				backgroundRect
					.selectAll(`.${clazz}-node`)
					.data(viewGraph.nodes, x => x.id)
					.join(enter=>{
						enter.append('rect')
							.attr('class', d=>`${clazz}-node ${clazz}-node-${d.type}`)
							.attr('id',d=>d.id)
							.on('click', selectNode)
							.attr('fill', d=>cNodes[d.id]!==undefined ? 'white' : 'lightgray') 
							.attr('stroke', d=>cNodes[d.id]!==undefined ? d.color : 'gray')
							.attr('stroke-width', d=>4)
							.attr('opacity', n=>cNodes[n.id]!==undefined ? NODE_OPACITY_SELECTED: NODE_OPACITY_UNSELECTED)
							.attr('rx', 5)		
							.attr('x', d=>d.parentId && vNodesMap[d.parentId]  ? vNodesMap[d.parentId].x0: 0)
							.attr('y', d=>d.parentId && vNodesMap[d.parentId] ? vNodesMap[d.parentId].y0: 0)
							.attr('width', d=>0)
							.attr('height', d=>0)
							.transition().duration(1000)
							.attr('x', d=>d.x0)
							.attr('y', d=>d.y0-costScale(d))
							.attr('width', d=>d.x1-d.x0)
							.attr('height', d=>(d.y1-d.y0)+costScale(d)*2);
						},update=>update
							.transition().duration(1000)
								.attr('fill', d=>cNodes[d.id]!==undefined ? 'white' : 'lightgray') 
								.attr('stroke', d=>cNodes[d.id]!==undefined ? d.color : 'gray')
								.attr('x', d=>d.x0)
								.attr('y', d=>d.y0-costScale(d))
								.attr('width', d=>d.x1-d.x0)
								.attr('height', d=>d.y1-d.y0+costScale(d)*2)
						,exit=>exit
							.transition().duration(1000)
								.attr('x', d=>d.parentId && vNodesMap[d.parentId] ? vNodesMap[d.parentId].x0: 0)
								.attr('y', d=>d.parentId && vNodesMap[d.parentId]  ? vNodesMap[d.parentId].y0: 0)
								.attr('width',d=>0)
								.attr('height',d=>0)
					);
			

				backgroundRect
					.selectAll(`.${clazz}-node-fo`)
					.data(viewGraph.nodes, x => x.id)
					.join(enter=>{
						
						let fo = enter.append('foreignObject')
							.attr('id',d=>d.id)
							.attr('class', `${clazz}-node-fo`)
							.attr('font-size', `${nodeFont.size}em`)
							.attr('opacity',1)
							.attr('width', d=>d.x1-d.x0)
							.attr('height', d=>(d.y1-d.y0)+costScale(d)*2)
							.attr('x',d=>d.x0)
							.attr('y',d=>d.y0)
							.on('click', selectNode);
						
						fo.append('xhtml:div')
							.append('div')
							.attr('id',d=>d.id)
							.attr('class', `${clazz}-node-text`)
							.attr('xmlns','http://www.w3.org/1999/xhtml')
							//.style('background-color', d=>'red')
							.style('color', d=>!showLabels? 'black': 'black')													
							.attr('width', d=>d.x1-d.x0)
							.text(d=>d.name);

						// fo.transition().duration(1000)
						// 	.attr('x', d=>(d.x0 ))
						// 	.attr('y', d=>d.y0-costScale(d))
							//.attr('opacity',1)
							//.attr('opacity', d=>cNodes[d.id]!==undefined ? LABEL_OPACITY_SELECTED: LABEL_OPACITY_UNSELECTED)
					},update=>{						
						update
						.transition().duration(1000)
							.attr('x',d=>d.x0)
							.attr('y',d=>d.y0);

						update.selectAll('*')
						.transition().duration(1000)
							.attr('x', d=>d.x0)
							.attr('font-size', `${nodeFont.size}em`)
							.attr('y', d=>d.y0-costScale(d))
							.attr('width', d=>d.x1-d.x0)
							.attr('height', d=>d.y1-d.y0+costScale(d)*2);
							//.attr('opacity', d=>cNodes[d.id]!==undefined ? LABEL_OPACITY_SELECTED: LABEL_OPACITY_UNSELECTED)
							
					},exit=>{
						exit.remove();
						// .selectAll('*').selectAll('*')
						// .transition().duration(1000)
						// .attr('x', d=>d.x0)
						// .attr('y', d=>d.y0-10)
						// .attr('width', 0)
						// .attr('height', 0)
						// .attr('opacity',0)
					});
				


					let linkG = backgroundRect.selectAll(`.${clazz}-link`)
					.data(viewGraph.links, x => x.id)
					.join(enter=>{
						enter.append('path')
								.attr('id',d=>d.id)
								.attr('class', `${clazz}-link`)
								.style('stroke', l=>l.circular ? 'green' : 'black')
								.style('stroke-width', d=>d.value)
								.style('fill', 'transparent') 
								.style('opacity',0)
								.attr('d', d=>d.path)
								.transition().duration(1000)								
									.style('opacity',l=>cLinks &&cLinks[l.id] ? LINK_OPACITY_SELECTED : LINK_OPACITY_UNSELECTED);
					},update=>{
						update
							//.transition().duration(500)
							.attr('d', d=>d.path)
							.style('stroke-width', d=>d.value);
					},exit=>{
						exit.style('stroke-width', 0);
					});
				
					backgroundRect.selectAll('.g-arrow')
						.data(viewGraph.links, x => x.id)
						.join(enter=>{
							enter.append('path')								
								.attr('class', 'g-arrow')
								.style('stroke-width', 1)
								.style('stroke', 'black')
								.style('fill', 'none')
								.style('stroke-dasharray', 10 + ',' + 4)
								.attr('d', d=>d.path)
								.style('opacity',MOTION_OPACITY_SELECTED);
							},update=>{
								update
									.attr('d', d=>d.path)
									.style('opacity',MOTION_OPACITY_SELECTED);
							},exit=>{
								exit.style('opacity',0);
							});
				
		}

		renderGraph();
		return ()=>{};
	},[nodeFilter,showNodes,showLabels,showLinks,visibleNodes,setVisibleNodes,dimensions]);

	return (
		<div ref={wrapperRef} style={{ marginBottom: '2rem' }}>
			<svg id={clazz}>
				<g className="zoom-background"></g>
			</svg>
		</div>
	);
};

