import React, {useEffect,useState,useRef} from 'react';
import {useRecoilValue} from 'recoil';
import {contextGraphState} from '../state/state';
import {convertExcelDateToJSDate} from '../util/dates';
import { Gantt } from './timeline-gantt-component';
import dateFormat from 'dateformat';

export const TimelineComponent = ({height='300px'}) => {

	const contextGraph = useRecoilValue(contextGraphState);

	const tasks = {
		data: [],
		links: []
	};

	if(contextGraph != null){
		tasks.data = contextGraph.nodes
			//.filter(n=>return n.width>0})
			.map((n,i)=>{
			let d = n.startDt!==undefined ? convertExcelDateToJSDate(n.startDt) : new Date();
			let start_date = dateFormat(d,'yyyy-mm-dd-mm');
			let duration = n.startDt!==undefined ? n.endDt-n.startDt : 0;
			return {id:n.id,start_date,duration,text:n.name, progress: 0,cost: n.cost,uncertainty: n.uncertainty};
		});
	}

	const style={
		height: `${height}`
	};

	return <div style={style}>
			<Gantt tasks={tasks}/>
		</div>;
};
