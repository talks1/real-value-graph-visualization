import React from 'react';
import {useRecoilState} from 'recoil';
import {labelState} from '../state/state';

export const LabelComponent =  () => {
	
	const [label,setlabel] = useRecoilState(labelState);

	function onChange(e){
		setlabel(!label);
	}
	return <button onClick={onChange}>{label? 'Hide Label' : 'Show Labels'}</button>;

};

