import React from 'react';
import {useRecoilState} from 'recoil';
import {focusState} from '../state/state';

export const FocusComponent =  () => {
	
	const [focus,setFocus] = useRecoilState(focusState);

	function onChange(e){
		setFocus(!focus);
	}
	return <button onClick={onChange}>{focus? 'Show All' : 'Show Selection'}</button>;
};

