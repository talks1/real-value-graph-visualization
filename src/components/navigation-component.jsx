import React from 'react';
import {useRecoilState} from 'recoil';
import {contextLevelState} from '../state/state';

export const NavigationComponent =  () => {

	const [contextLevel,setContextLevel] = useRecoilState(contextLevelState);

	function onChange(e){
		setContextLevel(contextLevel-1);
	}

	return <div>{ 
				contextLevel>1 
				&& 
				<button onClick={onChange}>Navigate to Parent</button>	
			}</div>;
};

