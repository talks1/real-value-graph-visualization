import React from 'react';
import {useSetRecoilState} from 'recoil';
import {layoutTypeState} from '../state/state';

let layoutTypes=['left','center'];

export const LayoutTypeComponent =  () => {
	
	const setLayoutType = useSetRecoilState(layoutTypeState);

	function onChange(e){
		setLayoutType(e.target.value);
	}

	return <select onChange={onChange}>
		{
			layoutTypes.map(n=><option key={n}>{n}</option>)
		}
	</select>;
};

