import React, { Component, useEffect, useRef } from 'react';
import { gantt } from 'dhtmlx-gantt';
import 'dhtmlx-gantt/codebase/dhtmlxgantt.css';
 
export const Gantt = ({tasks})=> {
    
    const ganttContainer = useRef();

    useEffect(()=>{
            //gantt.config.grid_width = 800;

            gantt.config.xml_date = '%Y-%m-%d';
            gantt.config.readonly = true;

            gantt.config.scales = [
                {unit: 'year', step: 1, format: '%F, %Y'},
            ];

            gantt.config.columns = [
                {name:'text',label:'Task name',width:300},
                //{name:"duration",   label:"Duration", align:"center" },
                {name:'cost',label:'Cost',align:'center',},
                {name:'uncertainty',label:'Uncertainty',align:'center'}
            ];

            gantt.config.layout = {
                rows:[
                    {
                       cols: [
                        {             
                          view: 'grid', 
                          scrollY:'scrollVer'
                        },
                        { resizer: true, width: 1 },
                        {
                          view: 'timeline', 
                          scrollY:'scrollVer'
                        },
                        {
                          view: 'scrollbar', 
                          id:'scrollVer'
                        }
                    ]}
                ]
            };

            gantt.init(ganttContainer.current);
            gantt.parse(tasks);
        });

    return <div
            ref={ganttContainer}
            style={ { width: '100%', height: '100%' } }
        ></div>;
};