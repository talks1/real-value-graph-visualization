import React from 'react';
import {useRecoilState,useSetRecoilState} from 'recoil';
import {heightState, widthState, nodeFontState, nodeWidthState, nodeHeightState,nodeRatioState, nodePaddingState,zoomState} from '../state/state';
import { UncontrolledDropdown,DropdownToggle,DropdownMenu,DropdownItem,InputGroup, InputGroupAddon, InputGroupText, Button,Input } from 'reactstrap';
import {useAnimation} from './sankey-flow-component';

const nodeFonts=['0.4','0.6','0.8','1'];

export const NodeFontComponent =  () => {	
	const setNodeFont = useSetRecoilState(nodeFontState);

	function onChange(e){
		setNodeFont({size: e.target.value});
	}

	return <select onChange={onChange}> {
			nodeFonts.map(n=><option key={n}>{n}</option>)
	}</select>;
};


let nodeWidths=['10','30','50','70','100'];

export const NodeWidthComponent =  () => {	
	const [NodeWidth,setNodeWidth] = useRecoilState(nodeWidthState);

	function onChange(e){
		setNodeWidth(parseInt(e.target.value));
	}

	return <select onChange={onChange}> {
			nodeWidths.map(n=><option key={n}>{n}</option>)
	}</select>;
};

let nodeHeights=['10','30','50','70','100'];

export const NodeHeightComponent =  () => {	
	const [NodeHeight,setNodeHeight] = useRecoilState(nodeHeightState);

	function onChange(e){
		setNodeHeight(parseInt(e.target.value));
	}

	return <select onChange={onChange}> {
			nodeHeights.map(n=><option key={n}>{n}</option>)
	}</select>;
};


let nodeRatios=[0.5,1,2];

export const NodeRatioComponent =  () => {	
	const [nodeRatio,setNodeRatio] = useRecoilState(nodeRatioState);

	function onChange(e){
		setNodeRatio(parseInt(e.target.value));
	}

	return <select onChange={onChange}> {
			nodeRatios.map(n=><option key={n}>{n}</option>)
	}</select>;
};

let nodePaddings=[1,5,10,20,30];

export const NodePaddingComponent =  () => {	
	const [nodePadding,setNodePadding] = useRecoilState(nodePaddingState);

	function onChange(e){
		setNodePadding(parseInt(e.target.value));
	}

	return <select onChange={onChange}> {
			nodePaddings.map(n=><option key={n}>{n}</option>)
	}</select>;
};

export const HeightComponent =  () => {	
	const [height,setHeight] = useRecoilState(heightState);

	function onChange(e){
		setHeight(parseInt(e.target.value));
	}
	return <Input  min={500} max={2000} type="number" step="100" onBlur={onChange} value={height} onChange={onChange} />;
	
};


export const AnimationComponent =  () => {	

	const [animationEnabled,setAnimationEnabled] = useAnimation();
	
	function onChange(){
		setAnimationEnabled(!animationEnabled());
	}
	return <Button onClick={onChange}>{animationEnabled() ? 'Disable Animation' : 'Enable Animation' }</Button>;
	
};