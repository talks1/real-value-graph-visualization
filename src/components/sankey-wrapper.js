
import React from 'react';
import {useRecoilState,useRecoilValue,useSetRecoilState} from 'recoil';
import {theGraphState, legendState} from '../state/state';
import {setContextNodes,SankeyFlowComponent} from './sankey-flow-component';

export const SankeyFlowWrapper = ({data,clazz,level,level1Type,height,layoutType, padding,prepare=true, filter})=>{
    setContextNodes([]);//emptyStack
	
    const [theGraph,setTheGraph]  = useRecoilState(theGraphState);
    
    let initialize = false;

    let graph = null;

    if(theGraph!=null){
        if(theGraph.reload){            
            setTheGraph( theGraph=> {let r = Object.assign({},theGraph);r.reload=false;return r;});            
            initialize=true;
            setContextNodes(()=>[]);
        }
        graph = {nodes: theGraph.nodes, links: theGraph.links};
    }
    else {
        graph = data;
    }

    return <SankeyFlowComponent graph={graph} initialize={initialize} level={level} level1Type={level1Type} clazz={clazz} height={height} layoutType={layoutType} padding={padding} filter={filter}/>;
};
