import React from 'react';
import {legendState} from '../state/state';

export const LegendComponent =  ({nodetypes=null}) => {
	
	let nt = nodetypes ?nodetypes : legendState.nodeTypes;

	let nodeTypesStyled = nt.map(t=>Object.assign({style: {color: t.color}},t));

	const XHTMLDiv = (p) => {
		const { children } = p;
		return React.createElement('xhtml:div', p, children);
	
	};

	return <svg width="200" height="400">
		{
			nodeTypesStyled.map((l,i)=>{

				let style = Object.assign({fill: 'white', strokeWidth: 1, stroke: l.style.color},l.style);
				return <g key={i} style={style}> 
						<rect x="100" y={i*30} width="100" height="20"/>
						<foreignObject x="100" y={i*30} width="100" height="20">
							<XHTMLDiv>
								<div xmlns="http://www.w3.org/1999/xhtml">{l.type}</div>
							</XHTMLDiv>
						</foreignObject>
				</g>;
			})
		}
	</svg>;
};

