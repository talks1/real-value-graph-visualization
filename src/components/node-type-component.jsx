import React from 'react';
import {useRecoilState} from 'recoil';
import {nodeTypeState} from '../state/state';

let nodetypes=['rect','icon','circle',];

export const NodeTypeComponent =  () => {
	
	const [nodeType,setNodeType] = useRecoilState(nodeTypeState);

	function onChange(e){
		setNodeType(e.target.value);
	}

	return <select onChange={onChange}>{
			nodetypes.map(n=><option key={n}>{n}</option>)
		}</select>;
};

