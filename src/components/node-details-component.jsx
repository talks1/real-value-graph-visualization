import React from 'react';
import {useRecoilValue} from 'recoil';
import {selectedNodeState} from '../state/state';
import {convertExcelDateToString} from '../util/dates';
import { Card,CardTitle,CardBody,Table } from 'reactstrap';

export const NodeDetailsComponent =  () => {
	
	const selectedNode = useRecoilValue(selectedNodeState);

	let records = [];

	if(selectedNode){
		records = [
			{
				key:1, name: 'Name', value: selectedNode.name
			},
			{
				key:1, name: 'Id', value: selectedNode.id
			},
			{
				key:1, name: 'Parent Id', value: selectedNode.parentId
			},
			{
				key:1, name: 'Level', value: selectedNode.level
			},
			{
				key:1, name: 'Ancestors', value: selectedNode.ancestorsCount
			},
			{
				key:2, name: 'Descendants', value: selectedNode.descendantsCount
			}
		];
	}

	return <>
		{ selectedNode &&
				<Card>
					<CardBody>
	   					<CardTitle>Node Metrics</CardTitle>	
						   <Table>
								<thead>
									<tr>									
									<th>Property</th>
									<th>Value</th>									
									</tr>
								</thead>
								<tbody>
									{
										records.map(r=>(
											<tr key={r.name}>
												<td>{r.name}</td>
												<td>{r.value}</td>
											</tr>
										))
									}
								</tbody>
						</Table>
					</CardBody>
				</Card>
		}
		</>;
};

