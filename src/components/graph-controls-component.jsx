import React from 'react';
import {useRecoilState} from 'recoil';
import {showNodesState,showLinksState,labelState} from '../state/state';

export const GraphControlsComponent =  () => {
	
	const [showNodes,setShowNodes] = useRecoilState(showNodesState);
	const [showLinks,setShowLinks] = useRecoilState(showLinksState);
	const [showLabels,setShowLabels] = useRecoilState(labelState);

	function onChangeNodes(e){
		setShowNodes(!showNodes);
	}
	function onChangeLinks(e){
		setShowLinks(!showLinks);
	}
	function onChangeLabels(e){
		setShowLabels(!showLabels);
	}
	return <div>
	<button onClick={onChangeNodes}>{showNodes? 'Hide Nodes' : 'Show Nodes'}</button>
	<button onClick={onChangeLinks}>{showLinks? 'Hide Links' : 'Show Links'}</button>
	<button onClick={onChangeLabels}>{showLabels? 'Hide Labels' : 'Show Labels'}</button>
	</div>;

};