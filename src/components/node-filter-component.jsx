import React, {useState} from 'react';
import {useRecoilState, useRecoilValue} from 'recoil';
import {nodeFilterState,linkFilterState, enrichedGraphState} from '../state/state';
import { Input } from 'reactstrap';


export const NodeFilterComponent =  ({projection, projectionName}) => {
	
	const enrichedGraph = useRecoilValue(enrichedGraphState);
	const [nodeFilter,setNodeFilter] = useRecoilState(nodeFilterState);
	const [filterValues,setFilterValues] = useState([]);

	function changeIteration(e){
		let projectionValue = e.target.value;

		let newFilterValues =[...filterValues];
		let index = newFilterValues.indexOf(projectionValue);
		if(index>=0){			
			newFilterValues.splice(index,1);
		}else {			
			newFilterValues.push(projectionValue);
		}		
		setFilterValues(newFilterValues);

		let newNodeFilter = { ...nodeFilter };
		let filterFunction = (projectionValue==='All') ? ()=>true : (n)=>{
			let projectionValue = projection(n).toString();
			let filterResponse = newFilterValues.indexOf(projectionValue)!==-1;			
			return filterResponse;
		};
		newNodeFilter[projectionName]= filterFunction;
		setNodeFilter(newNodeFilter);
	}

	let projectionValues = [];
	
	if(enrichedGraph && enrichedGraph.nodes) {
		let projectionMap = enrichedGraph.nodes.reduce((h,n)=>{			
			let projectionValue = projection(n);		
			h[projectionValue]=projectionValue;
			return h;
		},{});
		projectionValues = Object.values(projectionMap);
	}
	projectionValues.push('All');

	return <div>
	{projectionName}: 
	{
		projectionValues.map(n=><span key={n}>
			<Input type="checkbox" key={n} value={n} onChange={changeIteration}></Input>{n}
			</span>)
	}
	</div>;
};

export const LinkFilterComponent =  ({projection, projectionName}) => {
	
	const enrichedGraph = useRecoilValue(enrichedGraphState);
	const [linkFilter,setLinkFilter] = useRecoilState(linkFilterState);
	const [filterValues,setFilterValues] = useState([]);

	function changeIteration(e){
		let projectionValue = e.target.value;

		let newFilterValues =[...filterValues];
		let index = newFilterValues.indexOf(projectionValue);
		if(index>=0){			
			newFilterValues.splice(index,1);
		}else {			
			newFilterValues.push(projectionValue);
		}		
		setFilterValues(newFilterValues);

		let newLinkFilter = {...linkFilter};
		let filterFunction = (projectionValue==='All') ? ()=>true : (n)=>{
			let projectionValue = projection(n).toString();
			let filterResponse = newFilterValues.indexOf(projectionValue)!==-1;			
			return filterResponse;
		};
		newLinkFilter[projectionName]= filterFunction;
		setLinkFilter(newLinkFilter);
	}

	let projectionValues = [];
	
	if(enrichedGraph && enrichedGraph.links) {
		let projectionMap = enrichedGraph.links.reduce((h,l)=>{			
			let projectionValue = projection(l);		
			h[projectionValue]=projectionValue;
			return h;
		},{});
		projectionValues = Object.values(projectionMap);
	}
	projectionValues.push('All');

	return <div>
	{projectionName}: 
	{
		projectionValues.map(n=><span>
			<Input type="checkbox" key={n} value={n} onChange={changeIteration}></Input>{n}
			</span>)
	}
	</div>;
};

