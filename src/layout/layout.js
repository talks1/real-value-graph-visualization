import {stratify,hierarchy,pack,extent} from 'd3';
import {sankeyJustify,sankeyCenter,sankeyLinkHorizontal} from 'd3-sankey';
import {sankeyCircular} from 'd3-sankey-circular';

export function layout(layoutType,graph){
    let newgraph = Object.assign({},graph);
    newgraph.links = newgraph.links.map(l=>({source: l.source, target: l.target, value: l.value? l.value: 1}));
    newgraph  = layoutNodes(layoutType,newgraph);
    newgraph.links = newgraph.links.map(l=>{
        return Object.assign({
            x0: l.source.x,
            x1: l.target.x,
            y0: l.source.y,
            y1: l.target.y,
        },l);
    });
    return newgraph;
}

function layoutNodes(layoutType,graph){
    if(layoutType==='flow'){
        return layoutAsFlow(graph);
    } else if(layoutType==='treemap'){
        return layoutAsTreemap(graph);
    } else if(layoutType==='pack'){
        return  layoutAsPack(graph);
    }else { //absolute
        return  layoutAsAbsolute(graph);
    }
}
export function toHierarchy(nodes){

    let root = {id:999999,name: 'root', value: 1};
    let nnodes = nodes.map(n=>n.parentId===undefined? Object.assign({parentId: 999999},n) : Object.assign({},n));
    nnodes.push(root);
    return stratify()
        .id(d=>d.id)
        .parentId(d=>d.parentId)(nnodes);
}

function layoutAsFlow(graph){
    let nodes2 = graph.nodes.map(n=>({id: n.id, data: n}));
    let sankeyFactory = sankeyCircular()
        //.scale(0.5)
        .nodeWidth(5)
        .nodePadding(10)
        //.nodePaddingRatio(0.5)
        .size([100, 100])
        .nodeId(d=>d.id)
        .nodeAlign(sankeyJustify)
        //.nodeSort((a,b)=>a.id<b.id)
        .iterations(32);
    sankeyFactory.nodeValue(()=>{
            return 30;
        });

    //let sankeyData = sankeyFactory({nodes: nodes2, links: graph.links});
    let sankeyData = sankeyFactory(graph);
    //sankeyFactory.update(sankeyData)

    let sankeyNodes = sankeyData.nodes.map(n=>{
        n.r=n.x1-n.x0;
        n.x=n.x0+(n.x1-n.x0)/2;
        n.y=n.y0+(n.y1-n.y0)/2;
        return n;
    });
    let sankeyLinks = sankeyData.links;

    let depthExtent = extent(sankeyNodes, d=>d.depth );

    return {
        nodes: sankeyNodes,
        links: sankeyLinks,
        linkPath: sankeyLinkHorizontal
    };
}

function layoutAsPack(graph){
    let h = toHierarchy(graph.nodes);
    //let dh = hierarchy(h)
    h.sum(d=>1);
    let dp = pack().size([100,100]).padding(5);
    let viewnodes = dp(h).descendants().map(n=>Object.assign({x0:n.x,x1:n.x,y0:n.y,y1:n.y},n));
    let viewlinks = graph.links.map(l=>{
        let source = viewnodes.find(n=>{
            return n.id==l.source;
        });
        let target = viewnodes.find(n=>n.id==l.target);
        return {data:l,source,target};
    });
    return {
        nodes: viewnodes,
        links: viewlinks,
        linkPath: sankeyLinkHorizontal()
    };
}

function layoutAsTreemap(graph){
    return graph;
}

function layoutAsAbsolute(graph){

    let viewnodes= graph.nodes.map(n=>{
        let x = n.x ? n.x : parseInt(Math.random()*100);
        let y = n.y ? n.y : parseInt(Math.random()*100);
        return {
            id: n.id,
            x,
            x0:x,
            x1: x,
            y,
            y0:y,
            y1: y,
            r: 5
        };
    });

    let viewlinks = graph.links.map(l=>{
        let source = viewnodes.find(n=>{
            return n.id==l.source;
        });
        let target = viewnodes.find(n=>n.id==l.target);
        return {data:l,source,target};
    });

    return {
        nodes: viewnodes,
        links: viewlinks,
        linkPath: sankeyLinkHorizontal()
    };
}