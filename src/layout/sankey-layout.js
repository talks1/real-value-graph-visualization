import {sankeyLeft,sankeyJustify,sankeyCenter,sankeyLinkHorizontal} from 'd3-sankey';
//import {sankeyCircular} from 'd3-sankey-circular'

import sankeyCircular from './sankeyCircular';
import {max} from 'd3';

export function layout(options){
    let {graph,dimensions,nodeWidth,nodeHeight,nodeRatio,nodePadding,layoutType='center'}=options;
    let newgraph=Object.assign({},graph);  
    newgraph.links = newgraph.links.map(l=>({source: l.source, target: l.target, value: l.value? l.value: 1}));
    newgraph  = layoutAsFlow({graph:newgraph,dimensions,nodeWidth,nodeHeight,nodeRatio,nodePadding,layoutType});
    newgraph.links = newgraph.links.map(l=>{
        return Object.assign({
                x0: l.source.x,
                x1: l.target.x,
                y0: l.source.y,
                y1: l.target.y,
            },l);
    });
    return newgraph;
}

// determines the number of rows/columns in the laid out graph
export function deriveRowsColumns(nodes){
    let columnsMap = nodes.reduce((h,n)=>{
        if(!h[n.x0])
            h[n.x0]=[n];
        else 
            h[n.x0].push(n);
        return h;
    },{});
    let columns = Object.keys(columnsMap).length;
    let rows = max(Object.values(columnsMap),d=>d.length);
    let result = {
        columns,
        rows
    };
    return result;
}

function deriveSizeFromLayout(nodes,nodeWidth,nodeHeight){
    let { rows, columns } = deriveRowsColumns(nodes);
    let result = {
        width: 2*columns*nodeWidth,
        height: 2*rows*nodeHeight 
    };
    return result;
}

function layoutAsFlow(options){
    const {graph,dimensions,nodeWidth,nodeHeight,nodeRatio,nodePadding,layoutType} = options;

    let sankeyFactory = sankeyCircular()
        .nodeWidth(nodeWidth)
        .nodePadding(nodePadding)
        .nodePaddingRatio(nodeRatio)
        .size([1000, 1000])
        .nodeId(d=>d.id)
        .nodeAlign(sankeyLeft)
        .iterations(32);
        // .nodeValue((n)=>{
        //     return n.cost ? n.cost : 0
        //     //return 100
        // })

    let sankeyData = sankeyFactory(graph);
    
    let dims = deriveSizeFromLayout(sankeyData.nodes,nodeWidth,nodeHeight);
    
    //let dheight = dims.height > dimensions.height ? dims.height: dimensions.height
    let dheight = dims.height;
    
    sankeyFactory = sankeyCircular()
        .nodeWidth(nodeWidth)
        .nodePadding(nodePadding)
        .nodePaddingRatio(nodeRatio)        
        .size([dims.width,dheight])
        .nodeId(d=>d.id)
        .nodeAlign(sankeyCenter)
        .iterations(32);
        // .nodeValue((n)=>{
        //     return n.cost ? n.cost : 0
        //     //return 100
        // })

    sankeyData = sankeyFactory(graph);

    let sankeyNodes = sankeyData.nodes;
    let sankeyLinks = sankeyData.links;

    sankeyLinks = sankeyLinks.map(l=>{l.id=`${l.source.id}-${l.target.id}`; return l;});

    return {
        dimensions: dims,
        nodes: sankeyNodes,
        links: sankeyLinks,
    };
}
