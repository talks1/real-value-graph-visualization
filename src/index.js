
import { SankeyFlowComponent } from './components/sankey-flow-component';
import { SankeyFlowWrapper } from './components/sankey-wrapper';
import { NodeDetailsComponent } from './components/node-details-component';
import { NodeFilterComponent, LinkFilterComponent } from './components/node-filter-component';
import { NodeTypeComponent } from './components/node-type-component';
import { LayoutTypeComponent } from './components/layout-type-component';
import { NodeFontComponent,NodeWidthComponent, NodeHeightComponent, NodeRatioComponent, NodePaddingComponent, HeightComponent, AnimationComponent } from './components/sankey-selection-component';
import { FocusComponent } from './components/focus-component';
import { LabelComponent } from './components/label-component';
import { NavigationComponent } from './components/navigation-component';
import { LegendComponent } from './components/legend-component';
import { TimelineComponent } from './components/timeline-component';
import { GraphControlsComponent } from './components/graph-controls-component';

export * from './state/state';

export const SankeyComponent = SankeyFlowComponent;
export const SankeyView = SankeyFlowWrapper;
export const NodeTypeControl = NodeTypeComponent;
export const LayoutTypeControl = LayoutTypeComponent;
export const NodeFontControl = NodeFontComponent;
export const NodeWidthControl = NodeWidthComponent;
export const NodeHeightControl = NodeHeightComponent;
export const NodeRatioControl = NodeRatioComponent;
export const NodePaddingControl = NodePaddingComponent;
export const HeightControl = HeightComponent;
export const AnimationControl = AnimationComponent;

export const GraphControls = GraphControlsComponent;
export const FocusControl = FocusComponent;
export const NavigationControl = NavigationComponent;
export const LabelControl = LabelComponent;
export const LegendControl = LegendComponent;
export const NodeFilterControl = NodeFilterComponent;
export const LinkFilterControl = LinkFilterComponent;

export const NodeDetailsControl = NodeDetailsComponent;

export const TimelineControl = TimelineComponent;