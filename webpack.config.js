const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
var HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const pkg = require('./package.json')
const DEMO_SOURCE_DIR=process.env.DEMO_SOURCE_DIR || 'demo'
console.log(`Demo Source Dir: ${DEMO_SOURCE_DIR}`)

const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: path.join(__dirname, `./${DEMO_SOURCE_DIR}/src/index.html`),
    filename: `./real-value-visualization-components.${pkg.version}.html`,
    inlineSource: '.(js|css|png)$' // embed all javascript and css inline
});
const htmlWebpackInlineSourcePlugin = new HtmlWebpackInlineSourcePlugin()

module.exports = {
    entry: path.join(__dirname, `./${DEMO_SOURCE_DIR}/src/index.js`),
    output: {
        path: path.join(__dirname, `./${DEMO_SOURCE_DIR}/dist`),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-react"]
                    }
                },
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
               test: /\.(png|svg|jpg|gif)$/,
               use: [
                 'url-loader',
               ],
             },
            {
                test: /\.mdx?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env',"@babel/preset-react"]
                        }
                    },
                    {
                        loader: 'real-value-deck/src/mdxdeck-loader',
                        options: {
                            remarkPlugins: [],
                        }
                    },
                    // {
                    //     loader: '@mdx-js/loader',
                    // },
                ]
            }
        ]
    },
    plugins: [htmlWebpackPlugin,htmlWebpackInlineSourcePlugin],
    resolve: {
        extensions: [".js", ".jsx", ".png"]
    },
    devServer: {
        port: 3006
    }
};


