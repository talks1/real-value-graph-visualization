# Real Value Visualization Components

This project has graph visualization components .

## Component Scenarios

The components published from this library exist in the `src` directory.
There is a demo directory containing mdx deck based presentation showing the components in action and used to develop the components.

## How to develop

Run
```
npm run dev
```
which runs the demo mdx deck presentation on a port defined in the webpack.config.js file.

## How to publish

This will produce an installable npm tgz file which can be copied to another project and installed.
```
npm pack
```

## Self Contained Distribution

This command will produce a self contained html file.
```
npm run prod
```
Please open the self contained html in `demo/dist`

## Technology Choices

### Circular Sankey 
[Here is one visualization ](https://bl.ocks.org/tomshanley/874923fe54b173735b456479423ac7d6) I have been attempting to adapt.  It utilizes this [d3 fork library](https://github.com/tomshanley/d3-sankey-circular)

## Recoil

I am trying [Recoil](https://recoiljs.org/) as a state management library and it appears to be useful