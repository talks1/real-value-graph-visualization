import React from 'react';
import { render} from 'react-dom';
import { RecoilRoot} from 'recoil'

import './styles.css'

//https://mdxjs.com/
//https://www.diycode.cc/projects/jxnblk/mdx-deck
import * as themes from 'real-value-deck/src/themes/index.js'
import { components }  from 'real-value-deck/src/gatsbyplugin/index.js'

//import { Config } from 'geodocs-provision-automation/src/config'

import Content from './index.md'
const { SlideDeck } = components

const App = (props) => {
    return <RecoilRoot> 
        <SlideDeck
        location={location}
        theme={themes.aurecon2}
        navigate={()=>{}}
        animationMode='none'
        children={Content({components}).props.children}
        >
        </SlideDeck>
    </RecoilRoot>
}

render(<App />, document.querySelector('#demo'));

    