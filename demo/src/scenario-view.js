import React from 'react';
import { json } from 'd3';

export const ScenarioView =  ({graph}) => {
	
	return <div>
            <div>
                Nodes <code>{JSON.stringify(graph.nodes,null,' ')}</code>
            </div>
            <div>
                Links<code>{JSON.stringify(graph.links)}</code>
            </div>
    </div>
}

