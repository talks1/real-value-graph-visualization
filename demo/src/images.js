import Sample1 from '../images/Sample1.png'
import Sample2 from '../images/Sample2.png'
import Sample3 from '../images/Sample3.png'
import Sample4 from '../images/Sample4.png'

export const images = {
    Sample1,
    Sample2,
    Sample3,
    Sample4
}