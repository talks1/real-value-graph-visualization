import { images } from './images'
import { Steps , Invert, Split, SplitRight , AnimateSelect, ThemeSelect, Image } from 'real-value-deck/src/gatsbyplugin/components.js'

import {SankeyView, NodeTypeControl, LayoutTypeControl, NodeFontControl, NodeWidthControl, NodeHeightControl,NodeRatioControl, NodePaddingControl,LabelControl, FocusControl , NavigationControl, LegendControl, NodeFilterControl, NodeDetailsControl,NodeMetricsControl, TimelineControl,HeightControl, AnimationControl, GraphControls,LinkFilterControl} from '../../src'

import { scenario1, scenario2, scenario3, scenario4, scenario5, scenario6, scenario7, scenario8, scenario9, scenario10,scenario11 , scenario12, scenario13,scenario14} from './scenarios'

import { ScenarioView} from './scenario-view.js'




# Hierarchy with links at level 3
Ctl click to drill through

<ScenarioView graph={scenario14}/>

<GraphControls/>
<NodeFilterControl projection={n=>n.level} projectionName="Level" />
<LinkFilterControl projection={l=>l.relationship} projectionName="Relationship" />
<SankeyView data={scenario14} clazz="scenario14" layoutType="hierarchical-sankey" height="200"  prepare={false} padding={10} />
<NodeDetailsControl/>

----
# Hierarchy with links which surface from children
Ctl click to drill through

<ScenarioView graph={scenario13}/>
<SankeyView data={scenario13} clazz="scenario12" layoutType="hierarchical-sankey" height="200" prepare={false} padding={10} />

# Links with feedback
Given *node C to A*

<ScenarioView graph={scenario3} />

Renders as
<SankeyView data={scenario3} clazz="scenario3" layoutType="hierarchical-sankey"/>


---
# Hierarchy with no links
Ctl click to drill through

<ScenarioView graph={scenario12}/>

<SankeyView data={scenario12} clazz="scenario12" layoutType="hierarchical-sankey" height="200" prepare={false} padding={20}/>

---
## Demonstrating the ability to filter the graph

<ScenarioView graph={scenario7}/>
<SankeyView data={scenario7} clazz="scenario7" layoutType="hierarchical-sankey" height="200" filter={n=>n.parentId==='A'}/>

---

# Drill Through
Given *node A links to B and node A has child nodes A1 and A2 which are linked and B has children B1 and B2*

<ScenarioView graph={scenario7}/>

Renders as

<SankeyView data={scenario7} clazz="scenario7" layoutType="hierarchical-sankey" height="200"/>

# Nodes and Links
Given *node A links to B and B links to C*

<ScenarioView graph={scenario1}  width="1000" height="500"/>

Renders as

<SankeyView data={scenario1} level="1" clazz="scenario1" layoutType="hierarchical-sankey"/>

----

# Nodes have types

Given *a set of all node types*

<ScenarioView graph={scenario4} />

Renders as

<SankeyView data={scenario4} clazz="scenario4"/>

<LegendControl nodetypes={scenario4.nodes}/>

----


# Links with different values
Given *node A links to B with value = 10 and B links to C with value of 1*

<ScenarioView graph={scenario2} />

Renders as

<SankeyView data={scenario2} clazz="scenario2"/>

----
# Links into nodes with more children are weighted more strongly
Given *node B has children*

<ScenarioView graph={scenario8} />

Then *the link into B is weighted greater than link into C*

<SankeyView data={scenario8} clazz="scenario8"/>

----

# Links into nodes with more links are weighted more strongly
Given *node B has more outbound links*

<ScenarioView graph={scenario9}  />

Then *the links into B are weighted more strongly*

<SankeyView data={scenario9} clazz="scenario9"/>

----



# Node Relationships

Programme-Element-Detail links represent *containment from a budget* perspective.

Inter Level relationships represent *information/decisions produced by one node which are required by another node*

Some Nodes have hypothesis which are uncertain and become more certain by spending money.
The uncertainty of a starting a node is the sum of uncertainties upstream.

Some nodes have benefits which are uncertain and become more certain by spending the money 
The potential benefits of starting a node is the sum of benefits of this node and downstream multiplied with the uncertaintity of the node.


---

# Diagram Controls

Node Width: <NodeWidthControl/>
Node Height: <NodeHeightControl/>
Node Ratio: <NodeRatioControl/>
Node Padding: <NodePaddingControl/>
HeightControl: <HeightControl/>

---




<Header>
<h2>RRAP</h2>
</Header>

<Footer>
<h2>Aurecon</h2>
</Footer>