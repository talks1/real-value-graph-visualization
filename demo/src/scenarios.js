
const baseNodes = [{
    name: 'A',
    level: 1,
  },
  {
    name: 'B',
    level: 1,
    iteration: {id: 1}
  },{
    name: 'C',
    level: 1,
    iteration: {id: 2}
  },
  {
    name: 'D',
    level: 1,
    iteration: {id: 2}
  }, 
  {
    name: 'AA',
    level: 2,
    parentId: 'A',
    iteration: {id: 1}
  }, 
  {
    name: 'AB',
    level: 2,
    parentId: 'A',
    iteration: {id: 2}
  },
  {
    name: 'BB',
    level: 2,
    parentId: 'B',
    iteration: {id: 2}
  },

  {
    name: 'AAA',
    level: 3,
    parentId: 'AA',
    iteration: {id: 1}
  }, 
  {
    name: 'AAB',
    level: 3,
    parentId: 'AA',
    iteration: {id: 2}
  }, 
  {
    name: 'BBB',
    level: 3,
    parentId: 'BB',
    iteration: {id: 1}
  }, 
].map((n,i)=>{n.id=n.name;return n })

const baseLinks = [
  {source: 'A', target: 'B'},
  {source: 'B', target: 'C'},
  {source: 'C', target: 'D'},
  {source: 'C', target: 'B'},
  {source: 'AA', target: 'AB'},
  {source: 'AB', target: 'BB'},
  {source: 'AAA', target: 'AAB'},
  {source: 'AAB', target: 'BBB'},
]

export const copy = n =>Object.assign({},n)
export const setLinkValue = l=>Object.assign({value: 10},l)

export const scenario1 = {
  nodes: [{name: 'B',},{name: 'A'},{name: 'C'}].map((n,i)=>{n.id=n.name;return n }),
  links: [{source: 'A', target: 'B'},{source: 'B', target: 'C'}].map(setLinkValue)
}

export const scenario2 = {
  nodes: scenario1.nodes,
  links: scenario1.links.map(l=>({...l,value: l.source==='A' ? 10 :1}))
}

export const scenario3 = {
  nodes: scenario1.nodes,
  links: scenario1.links.concat({source: 'C', target: 'A'}).map(setLinkValue)
}

const nodeTypes = ["Decision","People Process","Model Process","Stakeholder","Knowledge","Model","Uncertainty"]
export const scenario4 = {
  nodes: nodeTypes.map((n,i)=>({id: i,name: n,type: n, color: `#${9-i}${i}${9-i}`})),
  links: []
}


export const scenario7 = {
  nodes: ["A","B"].map(n=>({id:n,name:n,level:1})).concat(["A1","A2"].map(n=>({id:n,name:n,level:2,parentId:'A'}))).concat(["B1","B2"].map(n=>({id:n,name:n,level:2,parentId:'B'})))
  .map(n=>({...n,color: 'blue'})),
  links: [{source: 'A',target:'B'},{source: 'A1',target:'A2'},{source: 'B1',target:'B2'}]
}

export const scenario8 = {
  nodes: ["A","B","C"].map(n=>({id:n,name:n,level:1}))
    .concat(["B1","B2"].map(n=>({id:n,name:n,level:2,parentId:'B'}))),
  links: [{source: 'A',target:'B'},{source: 'B',target:'C'},{source: 'B1',target:'B2'}]
}

export const scenario9 = {
  nodes: ["A","B","C","D","E"].map(n=>({id:n,name:n,level:1})),
  links: [{source: 'A',target:'B'},{source: 'B',target:'C'},{source: 'B',target:'D'},{source: 'B',target:'E'}]
}

export const scenario5 = {
  nodes: [
  {
    name: 'A',
    level: 1,
    iteration: {id: 1}
  },
  {
    name: 'B',
    level: 1,
    iteration: {id: 1}
  },{
    name: 'C',
    level: 1,
    iteration: {id: 2}
  },
  {
    name: 'D',
    level: 1,
    iteration: {id: 2}
  }, 
  {
    name: 'AA',
    level: 2,
    parentId: 'A',
    iteration: {id: 1}
  }, 
  {
    name: 'AB',
    level: 2,
    parentId: 'A',
    iteration: {id: 2}
  },
  {
    name: 'BB',
    level: 2,
    parentId: 'B',
    iteration: {id: 2}
  },

  {
    name: 'AAA',
    level: 3,
    parentId: 'AA',
    iteration: {id: 1}
  }, 
  {
    name: 'AAB',
    level: 3,
    parentId: 'AA',
    iteration: {id: 2}
  }, 
  {
    name: 'BBB',
    level: 3,
    parentId: 'BB',
    iteration: {id: 1}
  }, 
].map((n,i)=>{n.id=n.name;n.color='red';return n })
,
links: [
  {source: 'A', target: 'B', value: 10},
  {source: 'B', target: 'C', value: 20},
  {source: 'C', target: 'D', value: 10},
  {source: 'C', target: 'B', value: 30},
  {source: 'AA', target: 'AB', value: 10},
  {source: 'AB', target: 'BB', value: 10},
  {source: 'AAA', target: 'AAB', value: 10},
  {source: 'AAB', target: 'BBB', value: 10},
  
]
}

export const scenario6 = {
  nodes: [
    {
      name: 'Budget',
      color: 'Blue',
      tag: 'Plan'
    },
    {
      name: 'TimeFrame',
      color: 'Blue',
      tag: 'Plan'
    },
    {
      name: 'Resources',
      color: 'Blue',
      tag: 'Plan'
    },
    {
      name: 'WeeklyStatus',
      color: 'Blue',
      tag: 'Plan'
    },
    {
      name: 'PrjOrganization',
      color: 'Blue',
      tag: 'Plan'
    },
    {
      name: 'PMPlan',
      color: 'Red',
      tag: 'Plan'
    },

    {
      name: 'Stakeholders',
      color: 'Blue',
      tag: 'Discover'
    },
    {
      name: 'Workshops',
      color: 'Blue',
      tag: 'Discover'
    },
    {
      name: 'Requirements',
      color: 'Blue',
      tag: 'Discover'
    },
    {
      name: 'FrameworkRpt',
      color: 'Red',
      tag: 'Discover'
    },

    {
      name: 'Summaries',
      color: 'Blue',
      tag: 'Organize'
    },
    {
      name: 'Reviews',
      color: 'Blue',
      tag: 'Organize'
    },

    
    {
      name: 'Data Gathering',
      color: 'Blue',
      tag: 'Discover'
    },

    {
      name: 'Plan',
      color: 'green'
    },
  {
    name: 'Discover',
    level: 1,
    color: 'green'
  },
  {
    name: 'Organize',
    level: 1,
    color: 'green'
  },{
    name: 'Rationalize',
    level: 1,
    color: 'green'
  },
  {
    name: 'Design',
    level: 1,
    color: 'green'
  }, 
  {
    name: 'Drive',
    level: 1,
    color: 'green'
  }, 
  {
    name: 'Handover',
    level: 1,
    color: 'green'
  }, 
]
//.filter(n=>n.tag===undefined || n.tag==='Plan')
.map((n,i)=>{n.id=n.name;n.level=1;return n })

,
links: [
  {source: 'Budget', target: 'Plan', value: 1},
  {source: 'TimeFrame', target: 'Plan', value: 1},
  {source: 'Resources', target: 'Plan', value: 1},
  {source: 'Plan', target: 'WeeklyStatus', value: 1},
  {source: 'Plan', target: 'PrjOrganization', value: 1},
  {source: 'Plan', target: 'PMPlan', value: 1},
  
  {source: 'Stakeholders', target: 'Discover', value: 1},
  {source: 'Workshops', target: 'Discover', value: 1},
  {source: 'Requirements', target: 'Discover', value: 1},
  {source: 'Discover', target: 'FrameworkRpt', value: 1},

  
  //{source: 'Discover', target: 'Summaries', value: 1},
  {source: 'Summaries', target: 'Organize', value: 1},
  //{source: 'Discover', target: 'Reviews', value: 1},
  {source: 'Reviews', target: 'Organize', value: 1},
  
  {source: 'Plan', target: 'Discover', value: 20},
  {source: 'Discover', target: 'Organize', value: 20},
  {source: 'Organize', target: 'Rationalize', value: 20},
  {source: 'Rationalize', target: 'Design', value: 20},
  {source: 'Design', target: 'Drive', value: 20},
  {source: 'Drive', target: 'Handover', value: 20},

  
  {source: 'Organize', target: 'Discover', value: 5},
  {source: 'Design', target: 'Discover', value:5},
  {source: 'Rationalize', target: 'Discover', value: 5},
  {source: 'Drive', target: 'Discover', value: 5},

]
}

export const scenario10 = {
  nodes: [{name: 'A',cost:20},{name: 'B', cost: 10},{name: 'E'},{name: 'C',cost:15},{name: 'D'}].map((n,i)=>{n.id=n.name;return n }),
  links: [{source: 'A', target: 'B'},{source: 'B', target: 'C'},{source: 'C', target: 'A'}]
}

let iteration1 = {name: 'it1',level: 1,type:"Iteration"}
let iteration2 = {name: 'it2',level: 1,type:"Iteration"}
export const scenario11 = {
  nodes: [
    iteration1,iteration2,
    {name: 'A',level: 2,iteration: iteration1},{name: 'B',level: 2,iteration: iteration1},
    {name: 'C', level: 2,iteration: iteration2},{name: 'D',iteration: iteration2}]
    .map((n,i)=>{n.id=n.name;return n }),
  links: [{source: 'A', target: 'B', level: 2},{source: 'B', target: 'C',level: 2},{source: 'C', target: 'D',level: 2}]
}

export const scenario12 = {
  nodes: [
    {name: 'A',},
    {name: 'B'},
    {name: 'A1', parentId: 'A'},
    {name: 'A2', parentId: 'A'},
    {name: 'B1', parentId: 'B'}
  ].map((n,i)=>{n.id=n.name;n.color='green';return n }),
  links: []
}


export const scenario13 = {
  nodes: [
    {name: 'A',},
    {name: 'B'},
    {name: 'A1', parentId: 'A'},
    {name: 'A2', parentId: 'A'},
    {name: 'A3', parentId: 'A'},
    {name: 'B1', parentId: 'B'},
    {name: 'B2', parentId: 'B'},
    {name: 'B3', parentId: 'B'}
  ].map((n,i)=>{n.id=n.name;n.color='green';return n }),
  links: [
    {source:"A1", target: "B1"},
    {source:"A1", target: "A2"},
    {source:"A2", target: "A3"},
    {source:"A3", target: "A1"},
    {source:"B1", target: "B2"},
    {source:"B2", target: "B3"}
  ]
}

export const scenario14 = {
  nodes: [
    {name: 'A1',},
    {name: 'A2'},
    {name: 'A1-0', parentId: 'A1'},
    {name: 'A2-0', parentId: 'A2'},
    {name: 'A1-0-0', parentId: 'A1-0'},
    {name: 'A1-0-1', parentId: 'A1-0'},
    {name: 'A1-0-2', parentId: 'A1-0'},
    {name: 'A2-0-0', parentId: 'A2-0'},
    {name: 'A2-0-1', parentId: 'A2-0'}
  ].map((n,i)=>{n.id=n.name;n.color='green';return n }),
  links: [
    {source:"A1-0-0", target: "A1-0-1", relationship:'flow'},
    {source:"A1-0-1", target: "A1-0-2", relationship:'flow'},
    {source:"A1-0-2", target: "A1-0-0", relationship:'flow'},
    {source:"A1-0-1", target: "A2-0-0", relationship:'flow'},
    {source:"A2-0-0", target: "A2-0-1", relationship:'flow'},
  ]
}